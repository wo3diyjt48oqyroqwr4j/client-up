package main

import (
	"flag"
	"fmt"
	"github.com/0xAX/notificator"
	"github.com/getlantern/systray"
	"github.com/sparrc/go-ping"
	"io/ioutil"
	"sort"
	"strings"
	"sync"
	"time"
)

var (
	pingCount, pingTimeout, pollingRate int
	devicesFile                         string
	deviceList                          []string
	spam                                bool
	wg                                  sync.WaitGroup
	statChan                            chan string
	statMap                             map[string]bool
	mapMutex                            sync.Mutex
)

func deviceWatcher() {
	detectChange := func(v0 string, s0 bool) bool {
		// mutex locked
		if v1, t := statMap[v0]; t {
			if s0 == v1 {
				return false
			} else {
				return true
			}
		}
		return false
	}
	go func() {
		for {
			stat := <-statChan
			sp := strings.Split(stat, ",")
			var change bool
			if sp[1] == "DOWN" {
				mapMutex.Lock()
				change = detectChange(sp[0], false)
				statMap[sp[0]] = false
				mapMutex.Unlock()
			} else {
				mapMutex.Lock()
				change = detectChange(sp[0], true)
				statMap[sp[0]] = true
				mapMutex.Unlock()
			}
			if change && spam {
				not := fmt.Sprintf("Status of <span color='#9758ff'><i><b>%s</b></i></span> changed to %s", sp[0], sp[2])
				if err := sendNote(not); err != nil {
					fmt.Println(err)
				}
			}
		}
	}()
	go func() {
		for {
			getClientStatus(deviceList)
			time.Sleep(time.Second * time.Duration(pollingRate))
		}
	}()
}

func pingAddress(address string) (bool, error) {
	p, err := ping.NewPinger(address)
	if err != nil {
		return false, err
	}
	for x := 0; x < pingCount; x++ {
		p.Count = 1
		p.Timeout = time.Second * time.Duration(pingTimeout)
		p.Run()
		stats := p.Statistics()
		if stats.PacketLoss == 0 {
			return true, nil
		}
	}
	return false, nil
}

func getClientStatus(deviceArray []string) {
	for _, value := range deviceArray {
		wg.Add(1)
		data := strings.Split(value, ",")
		go func() {
			deviceName := data[0]
			pingResult, _ := pingAddress(data[1])
			if pingResult {
				ut := "<span color='#16AD1F'><i><b>UP</b></i></span>"
				statChan <- deviceName + ",UP," + ut
			} else {
				dt := "<span color='#FB1B19'><i><b>DOWN</b></i></span>"
				statChan <- deviceName + ",DOWN," + dt
			}
			wg.Done()
		}()
	}
	wg.Wait()
}

func sendNote(inputText string) error {
	if inputText != "" {
		notify := notificator.New(notificator.Options{
			AppName: "client-UP",
		})
		if err := notify.Push("Device Status", inputText, "", notificator.UR_NORMAL); err != nil {
			fmt.Println(err)
		}
	}
	return nil
}

func listDevices() ([]string, error) {
	var devices []string
	fileBytes, err := ioutil.ReadFile(devicesFile)
	if err != nil {
		return nil, err
	}
	lines := strings.Split(string(fileBytes), "\n")
	for _, value := range lines {
		if len(value) < 4 || strings.Contains(value, "#") {
			continue
		}
		devices = append(devices, value)
	}
	return devices, nil
}

func currentStats() string {
	var rn string
	var ul, dl []string
	var uc, dc int
	for key, value := range statMap {
		if value {
			uc++
			ul = append(ul,
				fmt.Sprintf("<span color='#B184FF'>%s</span>-------<span color='#16AD1F'><i><b>UP</b></i></span>\n", key))
		} else {
			dc++
			dl = append(dl,
				fmt.Sprintf("<span color='#B184FF'>%s</span>-------<span color='#FB1B19'><i><b>DOWN</b></i></span>\n", key))
		}
	}
	if (uc + dc) > 0 {
		rn = fmt.Sprintf("<span color='#16AD1F'>UP:%d</span> <span color='#FB1B19'>DOWN:%d</span> TOTAL:%d\n", uc, dc, uc+dc)
	}
	sortList := func(list []string) {
		sort.Strings(list)
		for _, value := range list {
			rn = rn + value
		}
	}
	sortList(ul)
	sortList(dl)
	if rn == "" {
		rn = "Gathering Information"
	}
	return rn
}

func main() {
	flag.IntVar(&pingCount, "count", 4, "ping count for each client")
	flag.IntVar(&pingTimeout, "timeout", 3, "ping timeout")
	flag.IntVar(&pollingRate, "polling", 3, "delay in sec between tests")
	flag.BoolVar(&spam, "spam", false, "yes, please spam me with notifications of device state changes")
	flag.StringVar(&devicesFile, "file", "devices.csv", "devices csv file")
	flag.Parse()
	statMap = make(map[string]bool)
	statChan = make(chan string)
	deviceList, _ = listDevices()
	go deviceWatcher()
	onExit := func() {
		if err := sendNote("exit"); err != nil {
			fmt.Println(err)
		}
	}
	or := func() {
		go func() {
			systray.SetIcon(Image)
			systray.SetTitle("client-up")
			systray.SetTooltip("client-up")
			check := systray.AddMenuItem("Status", "Show Current Status")
			systray.AddSeparator()
			exitItem := systray.AddMenuItem("exit", "?")
			for {
				select {
				case <-check.ClickedCh:
					go func() {
						mapMutex.Lock()
						if err := sendNote(currentStats()); err != nil {
							fmt.Println(err)
						}
						mapMutex.Unlock()
					}()
				case <-exitItem.ClickedCh:
					systray.Quit()
					return
				}
			}
		}()
	}
	systray.Run(or, onExit)
}
